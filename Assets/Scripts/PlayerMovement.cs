﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour {
	private Vector3 velocity = Vector3.zero;
	private Vector3 rotation = Vector3.zero;
	private Vector3 cameraRotation = Vector3.zero;

	Rigidbody body;

	[SerializeField]
	private Camera camera;

	public void Move(Vector3 newVelocity) {
		velocity = newVelocity;
	}

	public void Rotate(Vector3 newRotation) {
		rotation = newRotation;
	}

	public void RotateCamera(Vector3 newRotation) {
		cameraRotation = newRotation;
	}

	void PerformMovement() {
		if (velocity == Vector3.zero) {
			return;
		}

		body.MovePosition(body.position + velocity * Time.fixedDeltaTime);
	}

	void PerformRotation() {
		if (rotation == Vector3.zero) {
			return;
		}

		body.MoveRotation(body.rotation * Quaternion.Euler(rotation));

		if (camera) {
			camera.transform.Rotate(-cameraRotation);
		}
	}

	void Start() {
		body = GetComponent<Rigidbody>();
	}

	void FixedUpdate() {
		PerformMovement();
		PerformRotation();
	}
}
