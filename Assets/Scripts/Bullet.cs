﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public enum Operation {
		PLUS,
		MINUS
	};

	public float speed = 0f;
	public Color color = Color.black;
	public Operation operation = Operation.PLUS;

	void Start() {
		GetComponent<Renderer>().material.color = color;
	}

	// Update is called once per frame
	void Update () {
		float moveDistance = speed * Time.deltaTime;

		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit, moveDistance)) {
			Material colliderMaterial = hit.collider.GetComponent<Renderer>().material;
			if (operation == Operation.PLUS) {
				colliderMaterial.color = new Color(
					Math.Min(1f, colliderMaterial.color.r + color.r / 10),
					Math.Min(1f, colliderMaterial.color.g + color.g / 10),
					Math.Min(1f, colliderMaterial.color.b + color.b / 10)
				);
			} else {
				colliderMaterial.color = new Color(
					Math.Max(0f, colliderMaterial.color.r - color.r / 10),
					Math.Max(0f, colliderMaterial.color.g - color.g / 10),
					Math.Max(0f, colliderMaterial.color.b - color.b / 10)
				);
			}
			Debug.Log(colliderMaterial.color);
			GameObject.Destroy(gameObject);
		} else {
			transform.Translate(Vector3.forward * moveDistance);
		}


		//transform.position = transform.position + transform.forward * speed * Time.deltaTime;
	}
}
