﻿using System;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
public class PlayerController : MonoBehaviour {
	public Gun gun;

	[SerializeField]
	private float movementSpeed = 10f;

	[SerializeField]
	private float lookSensitivity = 10f;

	private PlayerMovement movement;

	void Start() {
		movement = GetComponent<PlayerMovement>();
	}

	void Update() {
		Vector3 movementDirection = transform.right * Input.GetAxisRaw("Horizontal") + transform.forward * Input.GetAxisRaw("Vertical");
		Vector3 movementVelocity = movementDirection.normalized * movementSpeed;

		Vector3 rotation = new Vector3(0f, Input.GetAxisRaw("Mouse X"), 0f) * lookSensitivity;

		Vector3 cameraRotation = new Vector3(Input.GetAxisRaw("Mouse Y"), 0f, 0f) * lookSensitivity;

		movement.Move(movementVelocity);
		movement.Rotate(rotation);
		movement.RotateCamera(cameraRotation);

		float scroll = Input.GetAxis("Mouse ScrollWheel");
		if (scroll > 0f) {
			gun.SwitchColorUp();
		} else if (scroll < 0f) {
			gun.SwitchColorDown();
		}

		if (Input.GetButtonDown("Fire1")) {
			gun.ShootPlus();
		} else if (Input.GetButtonDown("Fire2")) {
			gun.ShootMinus();
		}
	}
}
