﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {
	public Bullet bullet;
	public Transform model;
	public Transform muzzle;
	public Color shootingColor = Color.blue;

	public Color[] availableColors = {
		Color.red,
		Color.green,
		Color.blue
	};

	Camera playerCamera;

	[SerializeField]
	float fireSpeed = 20f;

	[SerializeField]
	float fireRange = 50f;

	public void Start() {
		playerCamera = GetComponentInParent<Camera>();
	}

	public void ShootPlus() {
		Shoot(Bullet.Operation.PLUS);
	}

	public void ShootMinus() {
		Shoot(Bullet.Operation.MINUS);
	}

	public void Shoot(Bullet.Operation operation) {
		Bullet newBullet = Instantiate(bullet, muzzle.position, muzzle.rotation) as Bullet;
		newBullet.speed = fireSpeed;
		newBullet.color = shootingColor;
		newBullet.operation = operation;
	}



	public void SwitchColorUp() {
		SetColor((Array.IndexOf(availableColors, shootingColor) + 1) % availableColors.Length);
	}

	public void SwitchColorDown() {
		SetColor((Array.IndexOf(availableColors, shootingColor) + availableColors.Length - 1) % availableColors.Length);
	}

	public void SetColor(int index) {
		shootingColor = availableColors[index];
		model.GetComponent<Renderer>().material.color = shootingColor;
	}

	public void Update() {
		Ray ray = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
		Debug.DrawRay(ray.origin, ray.direction * 100, Color.red);

		RaycastHit hit;
		bool isHit = Physics.Raycast(ray, out hit, 1000);

		Vector3 shootPoint = isHit ? hit.point : ray.origin + playerCamera.transform.forward * fireRange;
		//Bullet hitbullet = Instantiate(bullet, shootPoint, muzzle.rotation) as Bullet;
		//model.rotation = Quaternion.LookRotation(shootPoint);
		model.LookAt(shootPoint);
	}
}
